# README #

The following code contains a matrix (created with numpy package).

the matrix contains input as desbribed.

solution : 

1) get the packman location in order to calculate distances between different objects (ghosts,walls, etc).
	get the row and column locations into 2 variables.

2) start enumrate the matrix :
	a) get the row each time and calculate the distance with packman row var.
	b) get the column each time and calculate the distance with packman column var.
	
3) after having both distances, add them together for a new distance var.

4) print the location and distance for each node in the matrix.